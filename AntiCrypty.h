#pragma once

#include <filesystem>
#include <string>
#include <vector>

#define DIRECTORY 1
#define FILE 2
#define UNKNOWN -1
#define TWO_ARGS 3
#define ELF_HEADER_SIZE 4

namespace fs = std::filesystem;

void printUsage(); // print usage
int pathExists(std::string pathString); // check if path exists
std::vector<char> fileToBytesVector(std::string path, std::streampos bytesNum = 0); // convert file to vector of bytes
bool isElf(fs::path path); // check if file is ELF
bool isInfectedBoyerMoore(fs::path path, std::vector<char> signature); // check if file is infected using Boyer-Moore algorithm
bool isInfected(fs::path path, std::vector<char> signature); // check if file is infected using my own algorithm
void scan(std::string path, std::string signature); // scan a given directory
