# AntiCrypty
AntiCrypty is a program that searches for the "crypty" virus inside ELF files.
## Usage
#### Windows
```
AntiCrypty.exe <folder_name> <file_signature>
```
#### Linux
```
./AntiCrypty <folder_name> <file_signature>
```
## Requirements
- c++17 or above
- GCC 8 or above
## Build
#### Windows
Simply open a new solution in Visual Studio, add the files and run the program.
#### Linux
Make sure your VS Code is configured with gcc 8 or above and c++17 or above. You might need to change your include directory as well.
```
g++-8 *.cpp -o AntiCrypty -std=c++17 -lstdc++fs
```