#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include "AntiCrypty.h"

/*
Function will print the program's usage.
*/

void printUsage()
{
    std::cout << "Usage:" << std::endl << "    AntiCrypty <folder_name> <file_signature>" << std::endl;
}

/*
Function will check if path exists and return its type.
Input: path
Output: type - directory, file or unknown (doesn't exist)
*/

int pathExists(std::string pathString)
{
    const fs::path path(pathString);
    std::error_code ec;

    if (fs::is_directory(path, ec))
    {
        return DIRECTORY;
    }
    else if (fs::is_regular_file(path, ec))
    {
        return FILE;
    }
    return UNKNOWN;
}

/*
Function will read binary file and return vector of bytes representing the file.
Input: file path, number of bytes to read (if not set - read all file)
Output: vector of bytes
*/

std::vector<char> fileToBytesVector(std::string path, std::streampos bytesNum)
{
    int size = 0;
    std::vector<char> fileBytes;
    std::ifstream file(path, std::ios::in | std::ios::binary);

    if (file.is_open()) // avoid accessing opened files
    {
        if (!bytesNum)
        {
            file.seekg(0, std::ios::end); // seek to end of file
        }
        size = file.tellg() + bytesNum; // get file size
        fileBytes.resize(size);
        file.seekg(0, std::ios::beg); // seek to beginning of file
        file.read(&fileBytes[0], size);
        file.close();
    }
    return fileBytes;
}

/*
Function will check if file is an ELF by checking the file's header (checking only first 4 bytes).
Input: file path
Output: true or false
*/

bool isElf(fs::path path)
{
    std::vector<char> fileBytes = fileToBytesVector(path.string(), ELF_HEADER_SIZE); // read header only
    std::vector<char> elfHeader{ 0x7f, 'E', 'L', 'F' };

    if (fileBytes == elfHeader) // is ELF file
    {
        return true;
    }
    return false;
}

/*
Function will check if file contains a given signature using the Boyer-Moore searching algorithm.
Input: file path, vector of bytes - signature
Output: true or false
*/

bool isInfectedBoyerMoore(fs::path path, std::vector<char> signature)
{
    std::vector<char> fileBytes = fileToBytesVector(path.string());

    if (fileBytes.empty()) // file is empty or couldn't be opened
    {
        return false;
    }
    auto it = std::search(fileBytes.begin(), fileBytes.end(), std::boyer_moore_searcher(signature.begin(), signature.end())); // searching for the signature inside the file,
                                                                                                                              // using the Boyer-Moore algorithm
    return it != fileBytes.end();
}

/*
Function will check if file contains a given signature using an algorithm I came up with.
Input: file path, vector of bytes - signature
Output: true or false
*/

bool isInfected(fs::path path, std::vector<char> signature)
{
    std::vector<char> fileBytes = fileToBytesVector(path.string());

    if (fileBytes.empty()) // file is empty or couldn't be opened
    {
        return false;
    }

    for (int i = 0; i < fileBytes.size(); i++)
    {
        for (int j = 0; (j < signature.size()) && (i + j != fileBytes.size()); j++)
        {
            if (fileBytes[i + j] != signature[j])
            {
                break;
            }
            if (j == signature.size() - 1) // found signature
            {
                return true;
            }
        }
    }
    return false;
}

/*
Function will scan all files in a given directory to check if they are infected.
Input: directory path, file signature path
Output: None
*/

void scan(std::string path, std::string signature)
{
    auto directory = fs::recursive_directory_iterator(path); // iterator to the contents of a directory and its subdirectories
    std::vector<char> signatureBytes = fileToBytesVector(signature);

    for (const auto& element : directory)
    {
        if (!element.is_directory()) // avoid checking directories paths
        {
            if (isElf(element.path()))
            {
                if (isInfectedBoyerMoore(element.path(), signatureBytes)) // Boyer-Moore algorithm is much more efficient in most of the cases,
                                                                          // especially when dealing with large data, therefore I use it,
                                                                          // but you can also see my algorithm in the function isInfected(...)
                {
                    std::cout << "File " << element.path() << " is infected!" << std::endl;
                }
            }
        }
    }
}