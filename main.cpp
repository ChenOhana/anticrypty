#include <iostream>
#include "AntiCrypty.h"

int main(int argc, char** argv)
{
    if (argc != TWO_ARGS)
    {
        printUsage();
        exit(1);
    }
	
    if (pathExists(argv[1]) == DIRECTORY && pathExists(argv[2]) == FILE) // check passed arguments
    {
        std::cout << "Scanning started..." << std::endl;
        scan(argv[1], argv[2]);
        std::cout << "Finished scan!" << std::endl;
    }
    else
    {
        std::cout << "Invalid input! Folder or file name does not exist" << std::endl;
        exit(1);
    }
	
    return 0;
}